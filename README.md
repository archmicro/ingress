# Ingress
The repository contains manifests for the Ingress routes in the
Kubernetes cluster and CI/CD pipeline script. 

The following routes are available:
- site.local (hands over the frontend to the client)
- api.site.local (processes requests to the API Gateway)
- test.site.local (test route)

Requires the following rules in the hosts file: <br>
172.28.0.2      site.local <br>
172.28.0.2      api.site.local <br>
172.28.0.2      test.site.local <br>

*172.28.0.2 IP-address of the cluster. <br>
Address output: <br>
kubectl get ing -o jsonpath='{.items[0].status.loadBalancer.ingress[0].ip}'